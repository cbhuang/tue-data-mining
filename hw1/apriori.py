"""
Foundations of Data Mining Homework 1

Author: Chuan-Bin Huang

Ref:
- http://rasbt.github.io/mlxtend/user_guide/frequent_patterns/apriori/
- http://rasbt.github.io/mlxtend/user_guide/frequent_patterns/association_rules/#association-rules-generation-from-frequent-itemsets
- https://github.com/calee0219/Python3-Fp-growth

Tutorial (FP Growth):
- https://www.youtube.com/watch?v=yCbankIouUU
"""

import pandas as pd
import mlxtend.frequent_patterns as fp
from pprint import pprint
# convert categorical data to boolean if needed:
#
# from mlxtend.preprocessing import TransactionEncoder
#
# te = TransactionEncoder()
# te_ary = te.fit(dataset).transform(dataset)
# df = pd.DataFrame(te_ary, columns=te.columns_)
# df

# preprocession
raw_data = pd.read_csv("data.csv")
df = raw_data.drop(["Person"], axis=1).astype(bool)  # drop columns and convert the entire dataset to boolean
df

"""
1-a. Frequent Itemset 
===================================
"""

freq_itemsets = fp.apriori(df, min_support=0.4, use_colnames=True)
freq_itemsets


"""
1-b. Association Rules
==========================
"""
assoc = fp.association_rules(freq_itemsets, metric="confidence", min_threshold=0.8)
assoc.loc[:, ['antecedents', 'consequents', 'support', 'confidence']]


"""
2-a. Transaction dataset, sorted by frequency
===============================
"""

ls = []  # list of transaction data (ordered)
for i in range(df.shape[0]):
    ls_i = []
    for j in range(df.shape[1]):
        if df.iloc[i, j]:  # append if true
            ls_i.append(df.columns[j])
    ls.append(tuple(ls_i))
pprint(ls)

td = pd.DataFrame.sum(df).sort_values(ascending=False)
td


"""
2-b-e. 
===============================
"""

from fp_growth import find_frequent_itemsets
print("\nresults of FP-Tree:")
for itemset in find_frequent_itemsets(ls, minimum_support=0.4, include_support=True):
    print(itemset)
