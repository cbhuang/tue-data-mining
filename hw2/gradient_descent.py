"""
Gradient descent code for linear regression.

Uniform learning rate. (could be adaptive.)

Author: Chuan-Bin Huang
Date: 2018-10-08
"""

import numpy as np
import pandas as pd
#import matplotlib
#matplotlib.use("module://backend_interagg")
import matplotlib.pyplot as plt
import matplotlib.ticker as tk
import sklearn.preprocessing as pp

"""
Parameters
================
"""

"""Model Parameters: initial guess"""
params = np.array([0.0, 0.0], dtype=float)  # alpha, beta
np.random.seed(24)
params[0] = np.random.uniform(-1, 1)
params[1] = np.random.uniform(-1, 1)
print("[Info] parameters: [a={:.8f}, b1={:.8f}]".format(params[0], params[1]))

"""learning rate"""
learning_rate = 1e-1

"""Convergence Threshold"""
err_ratio = 1e-6
#err_abs = 1e-13   # not needed in this case

"""Quality Control"""
err_round = 1e-13
standardize = True

"""display"""
show_fig = True

"""
Data
==========
"""

# load data
df = pd.read_csv("data_raw.csv")
df = df.iloc[115:120, :]  # acquisition
N = len(df)  # number of records

# standardize and reshape
if standardize:
    Y = pp.scale(df["Y"].values.reshape((N, 1)))
    X = pp.scale(df["X"].values.reshape((N, 1)))
else:  # only reshape
    Y = df["Y"].values.reshape((N, 1))
    X = df["X"].values.reshape((N, 1))

"""
Main
============
"""


def mse(params, Y, X):
    """Mean Square Error of parameters

    Args:
        params (iterable): model parameters (alpha, beta_1, beta_2, ....)
        Y (array): column vector y[i], i=0..N-1
        X (array): column vectors [x1[i], x2[i], ...]

    Returns: tuple
    """

    N = len(Y)
    alpha = params[0]
    beta = params[1:]
    sse = 0
    for i in range(N):
        sse += (alpha + X[i, :].dot(beta) - Y[i])**2
    return sse / N


# Gradient of Error
def grad_mse(params, Y, X):
    """Gradient at theta (analytical expression)

    Args:
        params (iterable): model parameters (alpha, beta_1, beta_2, ....)
        Y (array): column vector y[i], i=0..N-1
        X (array): column vectors [x1[i], x2[i], ...

    Returns: tuple
    """
    n_params = len(params)
    N = len(Y)
    alpha = params[0]
    beta = params[1:]

    grad = np.zeros(n_params)
    for i in range(N):
        # common constant
        a = 2 / N * (alpha + X[i, :].dot(beta) - Y[i])
        # partial alpha
        grad[0] += a
        # partial beta1, beta2, ...
        for j in range(n_params - 1):  # total n_params-1 coefficients of x
            grad[j+1] += X[i, j] * a

    #return grad / lg.norm(grad)  # normalized
    return grad

# Initialize loop
params_now = params
mse_now = mse(params, Y, X)
counter = 0
arr_mse = np.zeros(10000)  # storage of loss function
arr_mse[0] = mse_now

while True:
    mse_prev = mse_now
    gradient = grad_mse(params_now, Y, X)

    # we don't know which direction to go, so have to try
    params_now1 = params_now - learning_rate * gradient
    params_now2 = params_now + learning_rate * gradient
    mse_now1 = mse(params_now1, Y, X)
    mse_now2 = mse(params_now2, Y, X)
    if mse_now1 < mse_now2:
        mse_now = mse_now1
        params_now = params_now1
    else:
        mse_now = mse_now2
        params_now = params_now2

    arr_mse[counter + 1] = mse_now

    # check progress
    #if counter % 10 == 0:
    #    print("[Debug] i={}: mse={}, a={:.3e}, b1={:.3e}".format(counter, mse_now, params_now[0], params_now[1]))
    #    print("        grad[0]={:.3e}, grad[1]={:.3e}".format(gradient[0], gradient[1]))
    s = "step {}: alpha={:.8f}, beta={:.8f}, grad[0]={:.8f}, grad[1]={:.8f}".format(counter, params_now[0], params_now[1], gradient[0], gradient[1])
    print(s)

    # check convergence
    if 1 - mse_now / mse_prev < err_ratio:
        break

    counter += 1

    # save first 3 plots
    if counter <= 3:
        # 1. figax
        fig = plt.figure(figsize=(8, 6), dpi=100)
        ax = fig.add_subplot(1, 1, 1)
        ax.set_title("Iteration = {}".format(counter), fontsize=20)
        # 2. plot
        y_hat = params_now[0] + X.dot(params_now[1:])
        ax.plot(X, Y, " +", label="Data")  # raw data
        ax.plot(X, y_hat, label="Regression Line")  # trend line
        # 3. label
        ax.set_xlabel("Degree Celcius (Standarized)", size=16)
        ax.set_ylabel("Power (Standarized)", size=16)
        # 4. range
        # ax.set_xlim(0, 40)
        # ax.set_ylim(400, 500)
        # 5. major ticks
        # ax.xaxis.set_major_locator(tk.MultipleLocator(10))
        # ax.yaxis.set_major_locator(tk.MultipleLocator(20))
        ax.tick_params(axis='both', which='major', labelsize=16)
        # # 6. minor ticks
        # 7. legend
        fig.legend(fontsize=14, bbox_to_anchor=(0.92, 0.92))
        # 8. adjust
        fig.tight_layout()
        # show
        if show_fig:
            fig.show()
        # save
        fig.savefig("Grad_step{}.png".format(counter))
        # close
        plt.close(fig)

        # save text
        with open("params_step{}.txt".format(counter), "w+") as f:
            f.write(s)

print("[Info] Iteration finished!")
print("       step {}: alpha={:.8f}, beta={:.8f}, grad[0]={:.8f}, grad[1]={:.8f}".format(counter, params_now[0], params_now[1], gradient[0], gradient[1]))
print(params_now)

arr_mse = arr_mse[arr_mse > 0.0]
print("      len(arr_mse) = {}".format(len(arr_mse)))

"""
Plot
============================
"""

# direct plot for model

# 1. figax
fig = plt.figure(figsize=(8, 6), dpi=100)
ax = fig.add_subplot(1,1,1)
ax.set_title("Gradient Descent (Standarized Data)", fontsize=20)
# 2. plot
y_hat = params_now[0] + X.dot(params_now[1:])
ax.plot(X, Y, " +", label="Data")  # raw data
ax.plot(X, y_hat, label="Regression Line")  # trend line
# 3. label
ax.set_xlabel("Degree Celcius (Standarized)", size=16)
ax.set_ylabel("Power (Standarized)", size=16)
# 4. range
#ax.set_xlim(0,40)
#ax.set_ylim(400, 500)
# 5. major ticks
#ax.xaxis.set_major_locator(tk.MultipleLocator(10))
#ax.yaxis.set_major_locator(tk.MultipleLocator(20))
ax.tick_params(axis='both', which='major', labelsize=16)
# 6. minor ticks
# 7. legend
fig.legend(fontsize=14, bbox_to_anchor=(0.92, 0.92))
# 8. adjust
fig.tight_layout()
# show
if show_fig:
    fig.show()
# save
fig.savefig("Grad_model.png")
# close
plt.close(fig)

if standardize:

    # transform back
    beta = np.std(df["Y"]) / np.std(df["X"]) * params_now[1]
    alpha = np.mean(df["Y"]) - beta * np.mean(df["X"])
    print("[Info] alpha={}, beta={}".format(alpha, beta))
    # analytical solution: Params=[496.57879424  -2.12603256]

    # 1. figax
    fig = plt.figure(figsize=(8, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title("Gradient Descent (Original Data)", fontsize=20)
    # 2. plot
    y_hat = alpha + beta * df["X"].values.reshape((N, 1))
    ax.plot(df["X"], df["Y"], " +", label="Data")  # raw data
    ax.plot(df["X"], y_hat, label="Regression Line")  # trend line
    # 3. label
    ax.set_xlabel("Degree Celcius", size=16)
    ax.set_ylabel("Power", size=16)
    # 4. range
    # ax.set_xlim(0,40)
    # ax.set_ylim(400, 500)
    # 5. major ticks
    # ax.xaxis.set_major_locator(tk.MultipleLocator(10))
    # ax.yaxis.set_major_locator(tk.MultipleLocator(20))
    ax.tick_params(axis='both', which='major', labelsize=16)
    # 6. minor ticks
    # 7. legend
    fig.legend(fontsize=14, bbox_to_anchor=(0.92, 0.92))
    # 8. adjust
    fig.tight_layout()
    # show
    if show_fig:
        fig.show()
    # save
    fig.savefig("Grad_original.png")
    # close
    plt.close(fig)


# Loss Function

# 1. figax
fig = plt.figure(figsize=(8, 6), dpi=100)
ax = fig.add_subplot(1,1,1)
ax.set_title("Loss Function", fontsize=20)
# 2. plot
arr_counter = np.array([i for i in range(len(arr_mse))])
ax.plot(arr_counter, arr_mse, " +", label="MSE")  # raw data
# 3. label
ax.set_xlabel("Iterations", size=16)
ax.set_ylabel("MSE", size=16)
# 4. range
#ax.set_xlim(0,40)
#ax.set_ylim(400, 500)
# 5. major ticks
#ax.xaxis.set_major_locator(tk.MultipleLocator(10))
#ax.yaxis.set_major_locator(tk.MultipleLocator(20))
ax.tick_params(axis='both', which='major', labelsize=16)
# 6. minor ticks
# 7. legend
#fig.legend(fontsize=14, bbox_to_anchor=(0.95, 0.95))
# 8. adjust
fig.tight_layout()
# show
if show_fig:
    fig.show()
# save
fig.savefig("MSE_Grad.png")
# close
plt.close(fig)

# Loss Function (log-scale)

# 1. figax
fig = plt.figure(figsize=(8, 6), dpi=100)
ax = fig.add_subplot(1,1,1)
ax.set_title("Loss Function (log scale)", fontsize=20)
# 2. plot
arr_counter = np.array([i for i in range(len(arr_mse))])
ax.plot(arr_counter, np.log10(arr_mse), " +", label="MSE")  # raw data
# 3. label
ax.set_xlabel("Iterations", size=16)
ax.set_ylabel("log(MSE)", size=16)
# 4. range
#ax.set_xlim(0,40)
#ax.set_ylim(400, 500)
# 5. major ticks
#ax.xaxis.set_major_locator(tk.MultipleLocator(10))
#ax.yaxis.set_major_locator(tk.MultipleLocator(20))
ax.tick_params(axis='both', which='major', labelsize=16)
# 6. minor ticks
# 7. legend
#fig.legend(fontsize=14, bbox_to_anchor=(0.95, 0.95))
# 8. adjust
fig.tight_layout()
# show
if show_fig:
    fig.show()
# save
fig.savefig("MSE_Grad_log.png")
# close
plt.close(fig)
