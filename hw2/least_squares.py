import numpy as np
import pandas as pd
import scipy.stats as st
import scipy.linalg as lg
from sklearn import preprocessing

df = pd.read_csv(r"data_raw.csv")
df = df.iloc[115:120, :]
n = df.shape[0]  # number of records

# Manual check
X = df["X"].values.reshape(n, 1)
Y = df["Y"].values.reshape(n, 1)

# data standardization
X = preprocessing.scale(X)
assert abs(np.mean(X)) < 1e-13 
assert abs(np.std(X)-1) < 1e-13
Y = preprocessing.scale(Y)
assert abs(np.mean(Y)) < 1e-13 
assert abs(np.std(Y)-1) < 1e-13

# closed form solution of standardized data
beta_std = (X.T).dot(Y) / (X.T).dot(X)

# transform back
beta = beta_std * np.std(df["Y"].values) / np.std(df["X"].values)
alpha = np.mean(df["Y"].values) - beta * np.mean(df["X"].values)

# check answer
# by scipy.stats suite
slope, intercept, rvalue, pvalue, stderr = st.linregress(df["X"], df["Y"])
assert abs(slope - beta) < 1e-13
assert abs(intercept - alpha) < 1e-13

# R^2
#y_hat = X.dot(theta)  # estimated y
#err = y_hat - y  # estimation error
#SSE = lg.norm(err)**2
#SST = lg.norm(y - np.mean(y))**2
#assert np.abs(R_sq - (1-SSE/SST)) < 1e-14, "[Error] R^2 value did not match!"

#R_sq = rvalue**2

print("[Info] slope={:8f}, intercept={:8f}".format(slope, intercept))

# plot using matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as tk
# 1. figax
fig = plt.figure(figsize=(8,6), dpi=100)
ax = fig.add_subplot(1,1,1)
ax.set_title("Least Squares (Original Data)", fontsize=20)
# 2. plot
y_hat = alpha + beta * df["X"].values
y_hat = y_hat.reshape((5,))
ax.plot(df["X"], df["Y"], " +", label="Data")  # raw data
ax.plot(df["X"], y_hat, label="Regression Line")  # trend line
# 3. label
ax.set_xlabel("Degree Celcius", size=16)
ax.set_ylabel("Data", size=16)
# 4. range
#ax.set_xlim(0,40)
#ax.set_ylim(400, 500)
# 5. major ticks
#ax.xaxis.set_major_locator(tk.MultipleLocator(10))
#ax.yaxis.set_major_locator(tk.MultipleLocator(20))
ax.tick_params(axis='both', which='major', labelsize=13)
# 6. minor ticks
# 7. legend
fig.legend(fontsize=14, bbox_to_anchor=(0.92, 0.92))
# 8. adjust
fig.tight_layout()
# show
fig.show()
# save
fig.savefig(r"LSQ.png")
# close
# plt.close(fig)

